from tkinter import *
import ctypes

class ChooseBox:
    def __init__(self):
        self.left = self.top = self.right = self.bottom = -1

        self.root = Tk()
        self.root.attributes('-fullscreen', True)
        self.root.attributes('-alpha', 0.5)
        self.root.title('Choose the binding box of the captions')

        #setting up a tkinter canvas with scrollbars
        self.frame = Frame(self.root, bd=2, relief=SUNKEN, bg='')
        self.frame.grid_rowconfigure(0, weight=1)
        self.frame.grid_columnconfigure(0, weight=1)
        self.canvas = Canvas(self.frame, bd=0)
        self.canvas.grid(row=0, column=0, sticky=N+S+E+W)
        self.frame.pack(fill=BOTH,expand=1)

        self.canvas.config(scrollregion=self.canvas.bbox(ALL))

        self.rect = self.canvas.create_rectangle(0,0,0,0)

        #mouseclick event
        self.canvas.bind("<Button 1>",self.printcoords)
        #mouse move event
        self.canvas.bind("<Motion>", self.on_move)

        self.scaleFactor = ctypes.windll.shcore.GetScaleFactorForDevice(0) / 100

        self.root.mainloop()

    def printcoords(self, event):
        if self.left == -1:
            self.left = event.x
            self.top = event.y
        else:
            self.right = event.x
            self.bottom = event.y

            if self.right < self.left:
                (self.left, self.right) =  (self.right, self.left)
            if self.bottom < self.top:
                (self.bottom, self.top) =  (self.top, self.bottom)


            self.left = int(self.left * self.scaleFactor)
            self.top = int(self.top * self.scaleFactor)
            self.right = int(self.right * self.scaleFactor)
            self.bottom = int(self.bottom * self.scaleFactor)
            self.root.destroy()

    def on_move(self, event):
        if self.left == -1:
            return
        self.canvas.delete(self.rect)
        self.rect = self.canvas.create_rectangle(self.left, self.top, event.x, event.y)
        # self.rect.pack()