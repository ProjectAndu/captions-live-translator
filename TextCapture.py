import threading

import cv2
import numpy as nm
from PIL import ImageGrab
from pytesseract import pytesseract
from deep_translator import GoogleTranslator
import os

import tkinter as tk

import time


class TextCapture(threading.Thread):
    def __init__(self, bbox, window, text_area):
        threading.Thread.__init__(self)
        pytesseract.tesseract_cmd = os.path.abspath(os.getcwd()) + r'\Tesseract-OCR\tesseract.exe'
        self.bbox = bbox
        # self.window = window
        # self.text_area = text_area


    def run(self):
        previous_text = ''
        while(True):
            cap = ImageGrab.grab(bbox=self.bbox)
            screenText = pytesseract.image_to_string(
                cv2.cvtColor(nm.array(cap), cv2.COLOR_BGR2GRAY),
                lang='fra')
            # Key pt limbi pt Translator:

            # 'fr': 'french',
            # 'ro': 'romanian',
            # 'en': 'english',
            translated_text = GoogleTranslator(source='fr', target='en').translate(screenText)
            if translated_text != previous_text:
                print(translated_text)
                # self.text_area.insert(tk.INSERT, translated_text)
                # self.text_area.update()
                previous_text = translated_text
            # time.sleep(0.01)
