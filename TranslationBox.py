
import tkinter as tk
from tkinter import scrolledtext as st


class TranslationBox():
    def __init__(self):
        self.win, self.text_area = self.createWindow()



    def createWindow(self):
        # Creating tkinter main window
        win = tk.Tk()
        win.resizable(width=True, height=True)
        win.title("ScrolledText Widget")
        label = tk.Label(text="Translator",
                         font=("Times New Roman", 15),
                         background='green',
                         foreground="white")
        label.pack()
        # Creating scrolled text area
        # widget with Read only by
        # disabling the state
        text_area = st.ScrolledText(win,
                                    height=10,
                                    width=15,
                                    font=("Times New Roman",
                                          15))
        text_area.pack(fill=tk.BOTH, expand=True)
        text_area.insert(tk.INSERT, 'asdasdasdsa')
        text_area.insert(tk.INSERT, 'asdasdasdsa')
        text_area.configure(state='disabled')
        text_area.focus()
        return win, text_area
